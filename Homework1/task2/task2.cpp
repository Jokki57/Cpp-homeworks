//
// Created by User on 11/12/2015.
//

#include "task2.h"

using namespace std;

void task2_init() {
    bool is_raining, will_raining, has_umbr, on_taxi, to_final_dest = false;
    is_raining = get_answer("Is it raining?");
    if (!is_raining) {
        will_raining = get_answer("Will be rain?");
    }
    if (!is_raining && !will_raining) {
        cout << "You don't need to take an umbrella";
        return;
    }
    has_umbr = get_answer("Do you have an umbrella?");
    if (!has_umbr) {
        cout << "You cannot take an umbrella";
        return;
    }
    on_taxi = get_answer("Will we go on taxi?");
    if (on_taxi) {
        to_final_dest = get_answer("Will we go on taxi to final destination?");
    }
    if (on_taxi && to_final_dest) {
        cout << "You don't need to take an umbrella";
    } else {
        cout << "You need to take an umbrella";
    }
}

int get_answer(string message) {
    cout << message << endl;
    int answ;
    cin >> answ;
    return answ;
}