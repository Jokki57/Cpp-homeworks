//
// Created by User on 11/12/2015.
//

#include <limits>
#include "task1.h"

using namespace std;

int hp1, hp2;

void task1_init() {
    hp1 = hp2 = 100;
    bool playerChooser = false;
    while (true) {
        if (!playerChooser) {
            hp2 -= get_damage_points(playerChooser);
        } else {
            hp1 -= get_damage_points(playerChooser);
        }
        if (hp1 <= 0 || hp2 <= 0) {
            break;
        }
        playerChooser = !playerChooser;
    }
    cout << get_players_name(playerChooser) + " has won";
}

int get_damage_points(int playerNumber) {
    cout << get_players_name(playerNumber) + " enters hit points: ";
    cin.sync();
    int input;
    cin >> input;
    return input;
}

string get_players_name(int player_number) {
    string message;
    if (!player_number) {
        message = "Player 1";
    } else {
        message = "Player 2";
    }
    return message;
}