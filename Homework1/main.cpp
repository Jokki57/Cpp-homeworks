#include <iostream>
#include "task1/task1.h"
#include "task2/task2.h"

using namespace std;

int main() {
	cout << "Choose task:\n1. Task 1\n2. Task 2\nEnter whar you choose: ";
	int choice = (char)cin.get() - '0';
	switch (choice) {
		case 1:
			task1_init();
			break;
		case 2:
			task2_init();
			break;
		default:
			break;
	}
	cin.sync();
	cin.get();
	return 0;
}